소스코드

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import NoSuchElementException

options = Options()
options.add_experimental_option('detach', True) # 브라우저 바로 닫힘 방지
options.add_experimental_option('excludeSwitches', ['enable-logging']) # 불필요한 메세지 제거

service = Service(ChromeDriverManager().install())

driver = webdriver.Chrome(service=service, options=options)

driver.get('https://dev.apmmust.com/wp-admin/admin.php?page=yith_wcbep_panel')

time.sleep(1)

def check_element_presence(driver, css_selector):
    try:
        driver.find_element(By.CSS_SELECTOR, css_selector)
        return True
    except NoSuchElementException:
        return False


login_input = driver.find_element(By.CSS_SELECTOR, "#user_login")
login_input.send_keys("fixfix")

pw_input = driver.find_element(By.CSS_SELECTOR, "#user_pass")
pw_input.send_keys("ZmyCR1RhkG3b0qBKbKPeMM")

login_button = driver.find_element(By.CSS_SELECTOR, "#wp-submit")
login_button.click()

time.sleep(1)

bulk_action_select = driver.find_element(By.CSS_SELECTOR, "#select2-yith-wcbep-bulk-action-container")
bulk_action_select.click()

time.sleep(1)

delete_permanently_option = driver.find_element(By.XPATH, "//*[text()='Delete permanently']")
delete_permanently_option.click()


def check_apply_confirm():
  try:
    all_checkbox = driver.find_element(By.CSS_SELECTOR, "#cb-select-all-1");
    all_checkbox.click()

    time.sleep(1)

    apply_button = driver.find_element(By.CSS_SELECTOR, "#yith-wcbep-products-table-apply-bulk-action")
    apply_button.click()

    time.sleep(1)  # Wait for 1 second

    confirm_button = driver.find_element(By.CSS_SELECTOR, ".yith-wcbep-confirm-delete-button")
    confirm_button.click()
  except NoSuchElementException:
    return

count = 521

while count > 0:
  time.sleep(3)
  count = count - 1

  try:
    overlay = driver.find_element(By.CSS_SELECTOR, ".blockUI.blockOverlay")
  except NoSuchElementException:
    check_apply_confirm() # overlay가 없는 경우에만 confirm을 한다

driver.quit()
